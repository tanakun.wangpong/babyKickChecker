import { Component, OnInit } from '@angular/core';
import { AddCountService } from '../service/add-count.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-view-history',
  templateUrl: './view-history.component.html',
  styleUrls: ['./view-history.component.css']
})
export class ViewHistoryComponent implements OnInit {
  Object = Object;
  babyKickList: any;
  constructor(
    private addCountService: AddCountService
  ) { }

  ngOnInit() {
    this.addCountService.getBabyKick().subscribe((res) => {
      const grouped = {};
      res.forEach(async (k) => {
        const date = (new Date(+ k.created_date + 1000 * 60 * 60 * 24)).toISOString().slice(0, 10);
        grouped[date] = [];
      });

      res.forEach(async (k) => {
        const date = (new Date(+ k.created_date + 1000 * 60 * 60 * 24)).toISOString().slice(0, 10);
        grouped[date].push(k);
      });

      const makeResult = [];
      Object.keys(grouped).forEach((date) => {
        const kick = grouped[date].filter(ele => ele.checking);
        const morning = kick.filter(ele => ele.phase === 'morning');
        const afternoon = kick.filter(ele => ele.phase === 'afternoon');
        const evening = kick.filter(ele => ele.phase === 'evening');
        makeResult.push({date: date, kick: kick, morning: morning.length, afternoon: afternoon.length, evening: evening.length});
      });
      const sortfiltered = makeResult.sort((a: any, b: any) =>
        new Date(a.date).getTime() - new Date(b.date).getTime()
      );
      this.babyKickList = sortfiltered;
      console.log(this.babyKickList);
    });
  }
  isHtmlPrintable(value) {
    return value === '' || typeof value === 'string' || typeof value === 'number';
  }
}
