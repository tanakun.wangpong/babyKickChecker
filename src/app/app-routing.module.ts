import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AddCountComponent } from './add-count/add-count.component';
import { ViewHistoryComponent } from './view-history/view-history.component';


const routes: Routes = [
  {
    path: '',
    component: AddCountComponent,
  },
  {
    path: 'view-history',
    component: ViewHistoryComponent,
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
