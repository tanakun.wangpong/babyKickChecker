import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Subject, BehaviorSubject, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { cp } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class AddCountService {
  babyKickRef: any;
  afs: any;
  babyKick$: any;
  size$ = new Subject<any>();
  constructor(afs: AngularFirestore) {
    this.afs = afs;
    this.babyKickRef = afs.collection('babyKick');
    this.babyKick$ = this.size$.pipe(switchMap(size => afs.collection('babyKick').valueChanges()));
  }

  getBabyKick(): Observable<any> {
    const subject = new BehaviorSubject([]);
    this.afs.collection('babyKick').valueChanges().subscribe(babyKick => {
      subject.next(babyKick);
    });
    return subject.asObservable();
  }

  addCount() {
    const today = new Date();
    const curHr = today.getHours();
    let phase = '';
    if (curHr < 12) {
      phase = 'morning';
    } else if (curHr < 16) {
      phase = 'afternoon';
    } else {
      phase = 'evening';
    }
    return this.babyKickRef.add({
      checking: true,
      phase: phase,
      created_date: Date.now()
    });
  }

  undoCount(lastBabyKick) {
    console.log('lastBabyKick=>', lastBabyKick);
    this.afs.doc('babyKick/' + lastBabyKick).delete().then(res => {
      console.log('del', res);
    });
  }
}
