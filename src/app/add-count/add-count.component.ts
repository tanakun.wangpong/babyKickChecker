import { Component, OnInit } from '@angular/core';
import { AddCountService } from '../service/add-count.service';

@Component({
  selector: 'app-add-count',
  templateUrl: './add-count.component.html',
  styleUrls: ['./add-count.component.css']
})
export class AddCountComponent implements OnInit {
  babyKickList: any;
  babyKickRemove: any;
  constructor(
    private addCountService: AddCountService
    ) {}

  ngOnInit() {
    this.addCountService.getBabyKick().subscribe((res) => {
      const filtered = res.filter(element => {
        return this.isToday(element.created_date) && element.checking;
      });
      const sortfiltered = filtered.sort((a: any, b: any) => a.created_date > b.created_date ? -1 : 1);
      this.babyKickList = sortfiltered;
    });
  }

  addCount() {
    this.addCountService.addCount().then((res) => {
      this.babyKickRemove = res.id;
    });
  }

  undoCount() {
    const lastTime = this.babyKickList[this.babyKickList.length - 1].created_date;
    console.log(Date.now() - lastTime);
    const min = this.millisToMinutes(Date.now() - lastTime);
    console.log('min=>', min);
    if (min > 3) {
      console.log('if');
      alert('Last kick is more than 3 minutes');
    } else {
      this.addCountService.undoCount(this.babyKickRemove);
    }
  }

  private isToday(someDate) {
    const date =  new Date(someDate);
    const today = new Date();
    return date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear();
  }

  private  millisToMinutes(millis) {
    const minutes = Math.floor(millis / 60000);
    // const seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes;
  }
}
