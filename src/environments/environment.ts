// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAEczjso3PbXo25iQ-aPWSCWmqTu114nDQ',
    authDomain: 'baby-kick-memo.firebaseapp.com',
    databaseURL: 'https://baby-kick-memo.firebaseio.com',
    storageBucket: 'baby-kick-memo.appspot.com',
    projectId: 'baby-kick-memo',
    messagingSenderId: '833416228429',
    appId: '1:833416228429:web:96a82b27af04905d2d3604',
    measurementId: 'G-XL0D7CJJJN'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
